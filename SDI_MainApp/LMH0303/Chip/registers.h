/*
 * registers.h
 *
 * Created: 12.10.2016 14:58:47
 *  Author: darya
 */ 


#ifndef REGISTERS_H_
#define REGISTERS_H_

/*!
 * \brief Namespace with the enum of register's names.
 */
namespace Register {
	enum Name { DeviceID, Status, Mask, Direction, Output, OutputCTRL, RSVD06, RSVD07, Test, Rev, TFPCount, TFNCount};
}

/*!
 * \brief Namespace with the enum of DeviceID register fields.
 */
namespace DeviceIDRegister{
	enum Field { DevID, RSVD };
}

/*!
 * \brief Namespace with the enum of Status register fields.
 */
namespace StatusRegister{
	enum Field { RSVD = 2, TFN, TFP, LOS };
}

/*!
 * \brief Namespace with the enum of Mask register fields.
 */
namespace MaskRegister{
	enum Field { SD = 6, RSVD6, PD, RSVD43, MTFN, MTFP, MLOS };
}

/*!
 * \brief Namespace with the enum of Direction register fields.
 */
namespace DirectionRegister{
	enum Field { HDTFThreshLSB = 13, SDTFThreshLSB, RSVD, DTFN, DTFP, DLOS };
}

/*!
 * \brief Namespace with the enum of Output register fields.
 */
namespace OutputRegister{
	enum Field { HDTFThresh = 19, AMP };
}

/*!
 * \brief Namespace with the enum of OutputCTRL register fields.
 */
namespace OutputCTRLRegister{
	enum Field { RSVD = 21, FLOSOF, FLOSON, LOSEN, MUTE, SDTFThresh };
}


/*!
 * \brief Namespace with the enum of RSVD06 register fields.
 */
namespace RSVD06Register{
	enum Field { RSVD = 27 };
}

/*!
 * \brief Namespace with the enum of RSVD07 register fields.
 */
namespace RSVD07Register{
	enum Field { RSVD = 28 };
}

/*!
 * \brief Namespace with the enum of Test register fields.
 */
namespace TestRegister{
	enum Field { CMPCMD = 29, RSVD };
}

/*!
 * \brief Namespace with the enum of Rev register fields.
 */
namespace RevRegister{
	enum Field { RSVD = 31, DIREV, PARTID };
}

/*!
 * \brief Namespace with the enum of TFPCount register fields.
 */
namespace TFPCountRegister{
	enum Field { RSVD = 34, TFPCount };
}

/*!
 * \brief Namespace with the enum of TFNCount register fields.
 */
namespace TFNCountRegister{
	enum Field { RSVD = 36, TFNCount };
}


#endif /* REGISTERS_H_ */