/*
 * IncFile1.h
 *
 * Created: 11/8/2016 9:11:03 AM
 *  Author: darya
 */ 


#ifndef INCFILE1_H_
#define INCFILE1_H_
#include <asf.h>

void * operator new(size_t size);

void operator delete(void * ptr);

#endif /* INCFILE1_H_ */