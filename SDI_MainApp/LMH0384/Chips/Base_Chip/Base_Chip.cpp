/* 
* Base_Chip.cpp
* Author: Roman
*/


#include "Base_Chip.h"

// default constructor
Base_Chip::Base_Chip(int chipID)
{
	this->chipID = chipID;
} //Base_Chip

// default destructor
Base_Chip::~Base_Chip()
{
} //~Base_Chip
