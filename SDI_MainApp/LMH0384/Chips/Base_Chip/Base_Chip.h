/*
* Base_Chip.h
* Author: Roman
*/


#ifndef __BASE_CHIP_H__
#define __BASE_CHIP_H__

#include <asf.h>
#include "../../API/Low_level_API/LMH0384_Low_API_Manager.h"

class Base_Chip
{
	//variables
	public:	
	LMH0384_Low_API_Manager* lowApiMgr; /**<An object of Low API manager class for reading/writing registers */
	protected:
	int chipID; /**<ID of the chip */
	
	
	private:

	//functions
	public:
	Base_Chip(int chipID);
	virtual ~Base_Chip();
	
	/**
	* Initialize the chip
	* @return Error code
	*/
	virtual int initChip() {return 0;}
	
	/**
	* Get the chip ID
	* @param buffer to load ID 
	* @return Error code
	*/
	virtual int getChipID(int* buf) {return 0;}
		
	/**
	* Read data from a register
	* @param address of the register
	* @param buffer to load register data 
	* @return Error code
	*/	
	virtual	int readRegister(uint8_t *regAddress, uint8_t* buf) {return 0;}
		
	/**
	* Read data from a register field
	* @param address of the register
	* @param address of the field
	* @param buffer to load register data 
	* @return Error code
	*/	
	virtual int readRegisterField(uint8_t* regAddress, uint8_t* field, uint8_t* buf) {return 0;}
	protected:

	/**
	* Set chip ID 
	* @param new chip ID
	* @return Error code
	*/	
	virtual int setChipID(int chipID) {return 0;}
		
	/**
	* Write data to a register
	* @param address of the register
	* @param data to write
	* @return Error code
	*/	
	virtual int writeRegister(uint8_t *regAddress, uint8_t* data) {return 0;}
		
	/**
	* Write data to a register field
	* @param address of the register
	* @param address of the field
	* @param data to writeS 
	* @return Error code
	*/		
    virtual int writeRegisterField(uint8_t* regAddress, uint8_t* field, uint8_t* data) {return 0;}
}; //Base_Chip

#endif //__BASE_CHIP_H__
