/*
* ATxmega128A4U_Chip.h
* Author: Roman
*/


#ifndef __LMH0384_CHIP__
#define __LMH0384_CHIP__

#include <asf.h>
#include "../Base_Chip/Base_Chip.h"

class LMH0384_Chip : public Base_Chip
{
	//variables
	public:
	protected:
	private:
	//LMH0384_High_API_Manager *highApiMgr;
	
	//functions
	public:
	LMH0384_Chip();
	~LMH0384_Chip();
	
	int initChip(int chipID);
	
	int getChipID(int* buf);
	int setChipID(int chipID);
	
	int readRegister(uint8_t *regAddress, uint8_t* buf);
	int readRegisterField(uint8_t* regAddress, uint8_t* field, uint8_t* buf);
	
	int writeRegister(uint8_t *regAddress, uint8_t* data);
	int writeRegisterField(uint8_t* regAddress, uint8_t* field, uint8_t* data);

	protected:
	private:
	
	

}; //LMH0384_Chip

#endif //__LMH0384_Chip_H__
