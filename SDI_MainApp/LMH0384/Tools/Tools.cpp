/*
* Tools.cpp
* Author: Roman
*/


#include "Tools.h"

// default constructor
Tools::Tools()
{
} //Tools

// default destructor
Tools::~Tools()
{
} //~Tools

int Tools::Calculate_cable_length_from_coeff(uint8_t coeff, double* res)
{
	//sorry for the magic numbers in advance, there's no reason not to use them
	double locRes = 0;
	if(coeff < 0)
	return -1;
	if(coeff >= 0 && coeff <= 20)
	{
		locRes = coeff * 10;
	}
	else if(coeff > 20 && coeff < 25)
	{
		locRes = (coeff - 20) * 30;
	}
	else if(coeff == 25)
	locRes = 400; // from 360 to 400 ???
	else
	locRes = -1; //coeff > 25
	
	res = &locRes;
	return 0;
}

char Tools::GetBit(uint8_t* bitArray, int index)
{
	//return (bitArray[index/8/*since uin8_t contains 8 bits*/] >> 7-(index & 0x7)) & 0x1;
	if(index < 0 || index > 7)
	return -1;
	return (*bitArray >> index) & 1;
}

int Tools::SetBit(uint8_t* bitArray, int index, uint8_t value)
{
	//bitArray[index/8] = bitArray[index/8/*since uin8_t contains 8 bits*/] | (value & 0x1) << 7-(index & 0x7);
	if(index < 0 || index > 7)
	return -1;
	switch(value)
	{
		case 0:
		*bitArray &= ~(1 << index);
		break;
		case 1:
		*bitArray |= 1 << index;
		break;
		default:
		return -1; //ERROR bit is 0 or 1 only!
		break;
	}
	
	return 0;
}