/*
* SPI_Manager.cpp
* Author: Roman
*/

#include "SPI_Manager.h"

// default constructor
SPI_Manager::SPI_Manager()
{
	
} //SPI_Manager

// default destructor
SPI_Manager::~SPI_Manager()
{
} //~SPI_Manager


// -----------public-------------
int SPI_Manager::Init_master (int chipID)
{
	spi = &SPIC; //according to the schematics
	Spi_init_pins ();
	Spi_init_module (chipID);
	
	return 0;
}

int SPI_Manager::Write_LMH0384_register (SPI_t *spi, int chipID, uint8_t* regAddress, uint8_t* dataBuffer, uint8_t* bitMask)
{
	//create a clean final data buffer
	uint8_t* finalDataBuffer = (uint8_t*)malloc(8);
	
	//0 bit is "0" - writing
	Tools::SetBit (regAddress, 7, 0);
	//uint8_t - 8 bits
	for(uint8_t i = 0, j = 0; i < 8; i++)
	{
		//if current field includes this bit (verify the index)
		if(bitMask[i] == 1)
		{
			//if data buffer runs out of the values or has incorrect values - error
			if(Tools::GetBit(dataBuffer, j) != 0 && Tools::GetBit(dataBuffer, j) != 1)
			{
				return -1; //ERROR
			}
			//else set next bit from dataBuffer at i position in finalData			
			char temp = Tools::GetBit(dataBuffer, j);
			Tools::SetBit (finalDataBuffer,i, temp);
			j++;
		}
	}
	//1-7 bits for reg address; 8-15 bits for data
	uint8_t writeBuffer[2] = {*regAddress, *finalDataBuffer};
	
	Select_device (spi, &spi_device_conf);
	Write_packet (spi, writeBuffer, 2);
	Deselect_device (spi, &spi_device_conf);
	
	return 0;
}

int SPI_Manager::Read_LMH3084_register(SPI_t *spi, int chipID, uint8_t* regAddress, uint8_t* read_buffer, uint8_t* bitMask)
{
	//0 bit is "1" - reading
	Tools::SetBit (regAddress, 7, 1);
	//1-7 bits for reg address
	uint8_t regAddressDummy[2] = {*regAddress, CONFIG_SPI_MASTER_DUMMY};
	
	Select_device (spi, &spi_device_conf);
	Read_packet (spi, regAddressDummy, read_buffer, 2);
	Deselect_device (spi, &spi_device_conf);
	
	//set all bits 0 except for the field bits
	//uint8_t - 8 bits
	for(uint8_t i = 0; i < 8; i++)
	{
		//if current field includes this bit (verify the index)		
		if(bitMask[i] == 0)
		{
			//set the bit from read_buffer at j position of finalReadBuffer
			//example: read_buffer[] = {0,0,1,1,0,0,1,0}, bitamask[] = {0,0,1,1,1,0,0,0} then finalReadBuffer = {0,0,1,1,0,0,0,0}
			Tools::SetBit (read_buffer, i, 0);			
		}		
	}	
	return 0;
}

// -------------private--------------
int SPI_Manager::Spi_init_pins ()
{
	ioport_configure_port_pin(&PORTC, PIN4_bm, IOPORT_INIT_HIGH | IOPORT_DIR_OUTPUT); //SDI_SPI_SSn_CH0 (PC4)
	ioport_configure_port_pin (&PORTC, PIN3_bm, IOPORT_INIT_HIGH | IOPORT_DIR_OUTPUT); //SDI_SPI_SSn_CH1 (PC3)
	ioport_configure_port_pin(&PORTC, PIN2_bm, IOPORT_INIT_HIGH | IOPORT_DIR_OUTPUT); //SDI_SPI_SSn_CH2 (PC2)
	ioport_configure_port_pin (&PORTB, PIN3_bm, IOPORT_INIT_HIGH | IOPORT_DIR_OUTPUT); //SDI_SPI_SSn_CH3 (PB3)		

	ioport_configure_port_pin(&PORTC, PIN5_bm, IOPORT_INIT_HIGH | IOPORT_DIR_OUTPUT); //mosi
	ioport_configure_port_pin(&PORTC, PIN6_bm, IOPORT_DIR_INPUT); //miso
	ioport_configure_port_pin(&PORTC, PIN7_bm, IOPORT_INIT_HIGH | IOPORT_DIR_OUTPUT); //sck
	
	return 0;
}

int SPI_Manager::Spi_init_module (int chipID)
{
	
	switch(chipID)
	{
		case 1:
		spi_device_conf.id = IOPORT_CREATE_PIN(PORTC, 4); //SDI_SPI_SSn_CH0 (PC4)
		break;
		case 2:
		spi_device_conf.id = IOPORT_CREATE_PIN(PORTC, 3); //SDI_SPI_SSn_CH1 (PC3)
		break;
		case 3:
		spi_device_conf.id = IOPORT_CREATE_PIN(PORTC, 2); //SDI_SPI_SSn_CH2 (PC2)
		break;
		case 4:
		spi_device_conf.id = IOPORT_CREATE_PIN(PORTB, 3); //SDI_SPI_SSn_CH3 (PB3)
		break;
		
	}
	spi_master_init(&SPIC);
	spi_master_setup_device(&SPIC, &spi_device_conf, SPI_MODE_0, 1000000, 0);
	spi_enable(&SPIC);
	
	return 0;
}

int SPI_Manager::Select_device (SPI_t *spi, struct spi_device *device)
{
	spi_select_device (spi, device);
	
	return 0;
}

int SPI_Manager::Deselect_device (SPI_t *spi, struct spi_device *device)
{
	spi_deselect_device (spi, device);
	
	return 0;
}

int SPI_Manager::Write_packet (SPI_t *spi, uint8_t *data, size_t len)
{
	spi_write_packet (spi, data, len);
	
	return 0;
}

int SPI_Manager::Read_packet (SPI_t *spi, uint8_t *regAddress, uint8_t* readBuffer, size_t len)
{
	spi_read_packetTry (spi, regAddress, readBuffer, len);
	
	return 0;
}

status_code_t SPI_Manager::spi_read_packetTry(SPI_t *spi, uint8_t* regAddress, uint8_t *readBuffer, size_t len)
{
	while (len--) {
		spi_write_single(spi, *regAddress++);		
		while (!spi_is_rx_full(spi)) {
		}
		spi_read_single(spi, readBuffer);
	}
	
	return STATUS_OK;
}

