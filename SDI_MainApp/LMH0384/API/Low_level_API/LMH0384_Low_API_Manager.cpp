/*
* LMH0384_Low_API_Manager.cpp
* Author: Roman
*/


#include "LMH0384_Low_API_Manager.h"

// default constructor
LMH0384_Low_API_Manager::LMH0384_Low_API_Manager(/*int chipIDParam*/)
{
	regs = new Registers();
	spiMgr = new SPI_Manager();
	//this->chipID = chipIDParam;
} //LMH0384_Low_API_Manager

// default destructor
LMH0384_Low_API_Manager::~LMH0384_Low_API_Manager()
{
	delete regs;
	delete spiMgr;
} //~LMH0384_Low_API_Manager

int LMH0384_Low_API_Manager::Init_SPI (int chipID)
{
	//Select_Device_By_ID(chipID);
	this->chipID = chipID;
	spiMgr->Init_master (chipID);
	return 0;
}

int LMH0384_Low_API_Manager::Select_Device_By_ID(int chipID)
{
	spiMgr->Select_device(spiMgr->spi, &spiMgr->spi_device_conf);
	return 0;
}

int LMH0384_Low_API_Manager::Deselect_Device_By_ID(int chipID)
{
	spiMgr->Deselect_device(spiMgr->spi, &spiMgr->spi_device_conf);
	return 0;
}

int LMH0384_Low_API_Manager::Write_LMH0384_ALL_Registers(uint8_t* dataGC, uint8_t* dataOutpDr, uint8_t* dataLanchAmp, uint8_t* dataCLI, uint8_t* dataDevID)
{
	Write_LMH0384_General_Control_Register (dataGC);
	Write_LMH0384_Output_Driver_Register (dataOutpDr);
	Write_LMH0384_Launch_Amplitude_Register (dataLanchAmp);
	
	return 0;
}

// ===== Set full registers ===

int LMH0384_Low_API_Manager::Write_LMH0384_General_Control_Register (uint8_t* data)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {1, 1, 1, 1, 1, 1, 1, 1};
	int error = spiMgr->Write_LMH0384_register(spiMgr->spi, this->chipID, regs->GENERAL_CONTROL_REGISTER_ADDRESS, data, bitMask);
	if(error == -1)
		return error;
	return 0;
}

int LMH0384_Low_API_Manager::Write_LMH0384_Output_Driver_Register (uint8_t* data)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {1, 1, 1, 1, 1, 1, 1, 1};
	int error = spiMgr->Write_LMH0384_register(spiMgr->spi, this->chipID, regs->OUTPUT_DRIVER_REGISTER_ADDRESS, data, bitMask);
	if(error == -1)
	return error;
	return 0;
}

int LMH0384_Low_API_Manager::Write_LMH0384_Launch_Amplitude_Register (uint8_t* data)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {1, 1, 1, 1, 1, 1, 1, 1};
	int error = spiMgr->Write_LMH0384_register(spiMgr->spi, this->chipID, regs->LAUNCH_AMPLITUDE_REGISTER_ADDRESS, data, bitMask);
	if(error == -1)
	return error;
	return 0;
}


// ===== Read registers =====


int LMH0384_Low_API_Manager::Read_LMH0384_General_Control_Register (uint8_t* buf)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {1, 1, 1, 1, 1, 1, 1, 1};
	int error = spiMgr->Read_LMH3084_register(spiMgr->spi, this->chipID, regs->GENERAL_CONTROL_REGISTER_ADDRESS, buf, bitMask);
	if(error == -1)
	return error;
	return 0;
}

int LMH0384_Low_API_Manager::Read_LMH0384_Output_Driver_Register (uint8_t* buf)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {1, 1, 1, 1, 1, 1, 1, 1};
	int error = spiMgr->Read_LMH3084_register(spiMgr->spi, this->chipID, regs->OUTPUT_DRIVER_REGISTER_ADDRESS, buf, bitMask);
	if(error == -1)
	return error;
	return 0;
}

int LMH0384_Low_API_Manager::Read_LMH0384_Launch_Amplitude_Register (uint8_t* buf)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {1, 1, 1, 1, 1, 1, 1, 1};
	int error = spiMgr->Read_LMH3084_register(spiMgr->spi, this->chipID, regs->LAUNCH_AMPLITUDE_REGISTER_ADDRESS, buf, bitMask);
	if(error == -1)
	return error;
	return 0;
}

// bits: (7, 6, 5, 4, 3)
int LMH0384_Low_API_Manager::Read_LMH0384_CLI_Register(uint8_t* buf)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {1, 1, 1, 1, 1, 1, 1, 1};
	int error = spiMgr->Read_LMH3084_register(spiMgr->spi, this->chipID, regs->CLI_REGISTER_ADDRESS, buf, bitMask);
	if(error == -1)
	return error;
	return 0;
}

// bits: (7, 6, 5, 4, 3, 2, 1, 0)
int LMH0384_Low_API_Manager::Read_LMH0384_DevID_Register(uint8_t* buf)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {1, 1, 1, 1, 1, 1, 1, 1};
	int error = spiMgr->Read_LMH3084_register(spiMgr->spi, this->chipID, regs->DEVICE_ID_REGISTER_ADDRESS, buf, bitMask);
	if(error == -1)
	return error;
	return 0;
}

// ===== Read\write regster fields =====

// bits: (7)
int LMH0384_Low_API_Manager::Read_LMH0384_GnrlCtrl_CD(uint8_t* buf)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 0, 0, 0, 0, 0, 1};
	int error = spiMgr->Read_LMH3084_register(spiMgr->spi, this->chipID, regs->GENERAL_CONTROL_REGISTER_ADDRESS, buf, bitMask);
	if(error == -1)
	return error;
	return 0;
}
// bits: (7)
int LMH0384_Low_API_Manager::Write_LMH0384_GnrlCtrl_CD(uint8_t* data)
{
	if(this->chipID < 0)
	return -1;

	uint8_t bitMask[8] = {0, 0, 0, 0, 0, 0, 0, 1};
	int error = spiMgr->Write_LMH0384_register(spiMgr->spi, this->chipID, regs->GENERAL_CONTROL_REGISTER_ADDRESS, data, bitMask);
	if(error == -1)
	return error;
	return 0;
}
// bits: (6)
int LMH0384_Low_API_Manager::Read_LMH0384_GnrlCtrl_Mute(uint8_t* buf)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 0, 0, 0, 0, 1, 0};
	int error = spiMgr->Read_LMH3084_register(spiMgr->spi, this->chipID, regs->GENERAL_CONTROL_REGISTER_ADDRESS, buf, bitMask);
	if(error == -1)
	return error;
	return 0;
}
// bits: (6)
int LMH0384_Low_API_Manager::Write_LMH0384_GnrlCtrl_Mute(uint8_t* data)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 0, 0, 0, 0, 1, 0};
	int error = spiMgr->Write_LMH0384_register(spiMgr->spi, this->chipID, regs->GENERAL_CONTROL_REGISTER_ADDRESS, data, bitMask);
	if(error == -1)
	return error;
	return 0;
}

// bits: (5)
int LMH0384_Low_API_Manager::Read_LMH0384_GnrlCtrl_Bypass(uint8_t* buf)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 0, 0, 0, 1, 0, 0};
	int error = spiMgr->Read_LMH3084_register(spiMgr->spi, this->chipID, regs->GENERAL_CONTROL_REGISTER_ADDRESS, buf, bitMask);
	if(error == -1)
	return error;
	return 0;
}

// bits: (5)
int LMH0384_Low_API_Manager::Write_LMH0384_GnrlCtrl_Bypass(uint8_t* data)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 0, 0, 0, 1, 0, 0};
	int error = spiMgr->Write_LMH0384_register(spiMgr->spi, this->chipID, regs->GENERAL_CONTROL_REGISTER_ADDRESS, data, bitMask);
	if(error == -1)
	return error;
	return 0;
}

// bits:  (4, 3)
int LMH0384_Low_API_Manager::Read_LMH0384_GnrlCtrl_SlpMode(uint8_t* buf)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 0, 1, 1, 0, 0, 0};
	int error = spiMgr->Read_LMH3084_register(spiMgr->spi, this->chipID, regs->GENERAL_CONTROL_REGISTER_ADDRESS, buf, bitMask);
	if(error == -1)
	return error;
	return 0;
}

// bits: (4, 3)
int LMH0384_Low_API_Manager::Write_LMH0384_GnrlCtrl_SlpMode(uint8_t* data)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 0, 1, 1, 0, 0, 0};
	int error = spiMgr->Write_LMH0384_register(spiMgr->spi, this->chipID, regs->GENERAL_CONTROL_REGISTER_ADDRESS, data, bitMask);
	if(error == -1)
	return error;
	return 0;
}

// bits:  (2)
int LMH0384_Low_API_Manager::Write_LMH0384_GnrlCtrl_Extended_3G_reach_mode(uint8_t* data)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 1, 0, 0, 0, 0, 0};
	int error = spiMgr->Write_LMH0384_register(spiMgr->spi, this->chipID, regs->GENERAL_CONTROL_REGISTER_ADDRESS, data, bitMask);
	if(error == -1)
	return error;
	return 0;
}

// bits:  (2)
int LMH0384_Low_API_Manager::Read_LMH0384_GnrlCtrl_Extended_3G_reach_mode(uint8_t* buf)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 1, 0, 0, 0, 0, 0};
	int error = spiMgr->Read_LMH3084_register(spiMgr->spi, this->chipID, regs->GENERAL_CONTROL_REGISTER_ADDRESS, buf, bitMask);
	if(error == -1)
	return error;
	return 0;
}

// bits: (7,6,5)
int LMH0384_Low_API_Manager::Read_LMH0384_OutputDriver_Output_swing (uint8_t* buf)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 0, 0, 0, 1, 1, 1};
	int error = spiMgr->Read_LMH3084_register(spiMgr->spi, this->chipID, regs->OUTPUT_DRIVER_REGISTER_ADDRESS, buf, bitMask);
	if(error == -1)
	return error;
	return 0;
}

// bits: (7,6,5)
int LMH0384_Low_API_Manager::Write_LMH0384_OutputDriver_Output_swing (uint8_t* data)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 0, 0, 0, 1, 1, 1};
	int error = spiMgr->Write_LMH0384_register(spiMgr->spi, this->chipID, regs->OUTPUT_DRIVER_REGISTER_ADDRESS, data, bitMask);
	if(error == -1)
	return error;
	return 0;
}

// bits: (4,3,2)
int LMH0384_Low_API_Manager::Read_LMH0384_OutputDriver_Offset_voltage (uint8_t* buf)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 1, 1, 1, 0, 0, 0};
	int error = spiMgr->Read_LMH3084_register(spiMgr->spi, this->chipID, regs->OUTPUT_DRIVER_REGISTER_ADDRESS, buf, bitMask);
	if(error == -1)
	return error;
	return 0;
}

// bits: (4,3,2)
int LMH0384_Low_API_Manager::Write_LMH0384_OutputDriver_Offset_voltage (uint8_t* data)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 1, 1, 1, 0, 0, 0};
	int error = spiMgr->Write_LMH0384_register(spiMgr->spi, this->chipID, regs->OUTPUT_DRIVER_REGISTER_ADDRESS, data, bitMask);
	if(error == -1)
	return error;
	return 0;
}

// bits: (7)
int LMH0384_Low_API_Manager::Read_LMH0384_LaunchAmpld_CoarseCtrl(uint8_t* buf)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 0, 0, 0, 0, 0, 1};
	int error = spiMgr->Read_LMH3084_register(spiMgr->spi, this->chipID, regs->LAUNCH_AMPLITUDE_REGISTER_ADDRESS, buf, bitMask);
	
	if(error == -1)
	return error;
	return 0;
}

// bits: (7)
int LMH0384_Low_API_Manager::Write_LMH0384_LaunchAmpld_CoarseCtrl(uint8_t* data)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 0, 0, 0, 0, 0, 1};
	int error = spiMgr->Write_LMH0384_register(spiMgr->spi, this->chipID, regs->LAUNCH_AMPLITUDE_REGISTER_ADDRESS, data, bitMask);
	if(error == -1)
	return error;
	return 0;
}

// bits: (6, 5, 4, 3)
int LMH0384_Low_API_Manager::Read_LMH0384_LaunchAmpld_FineCtrl(uint8_t* buf)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 0, 1, 1, 1, 1, 0};
	int error = spiMgr->Read_LMH3084_register(spiMgr->spi, this->chipID, regs->LAUNCH_AMPLITUDE_REGISTER_ADDRESS, buf, bitMask);
	if(error == -1)
	return error;
	return 0;
}

// bits: (6, 5, 4, 3)
int LMH0384_Low_API_Manager::Write_LMH0384_LaunchAmpld_FineCtrl(uint8_t* data)
{
	if(this->chipID < 0)
	return -1;
	
	uint8_t bitMask[8] = {0, 0, 0, 1, 1, 1, 1, 0};
	int error = spiMgr->Write_LMH0384_register(spiMgr->spi, this->chipID, regs->LAUNCH_AMPLITUDE_REGISTER_ADDRESS, data, bitMask);
	if(error == -1)
	return error;
	return 0;
}
