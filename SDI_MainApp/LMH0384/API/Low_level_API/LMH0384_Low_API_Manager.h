/*
* LMH0384_Low_API_Manager.h
*
* Created: 10/25/2016 7:00:34 PM
* Author: Roman
*/


#ifndef __LMH0384_LOW_API_MANAGER_H__
#define __LMH0384_LOW_API_MANAGER_H__

#include "../../Tools/Tools.h"
#include  "../../SPI_Manager/SPI_Manager.h"
#include "../../Registers/Registers.h"

/*!
 * \brief Low level API manager. Logic for reading and writing registers.
 */
class LMH0384_Low_API_Manager
{
	//variables
	public:
	Registers *regs; /**<A list of addresses of LMH0384 registers */
	
	protected:
	private:
	SPI_Manager *spiMgr; /**<Object of SPI manager class for collaboration with SPI interface */
	int chipID = -1; /**<Chip ID is -1 by default in case we didn't select any chip (leads to an error) */
	
	//functions
	public:
	LMH0384_Low_API_Manager(/*int chipIDParam*/);
	~LMH0384_Low_API_Manager();

	
	/**
	* Make slave selection and master initialization
	* @return The error code
	*/
	int Init_SPI(int chipID);

	/**
	* Make slave selection and master initialization
	* @return The error code
	*/
	int Select_Device_By_ID(int chipID);
	
	/**
	* Make slave selection and master initialization
	* @return The error code
	*/
	int Deselect_Device_By_ID(int chipID);
	
	// ===== Write registers =====
	
	/**
	* a normal member taking two arguments and returning an integer value.
	* @param dataGC an unsigned integer (8 bit) argument.
	* @param OutpDr an unsigned integer (8 bit) argument.
	* @param dataLanchAmp an unsigned integer (8 bit) argument.
	* @param dataCLI an unsigned integer (8 bit) argument.
	* @param dataDevID an unsigned integer (8 bit) argument.
	* @return The error code
	*/
	int Write_LMH0384_ALL_Registers (uint8_t* dataGC, uint8_t* dataOutpDr, uint8_t* dataLanchAmp, uint8_t* dataCLI, uint8_t* dataDevID);
	
	/**
	* Set the value from buffer "data" to General Control register
	* @param data an unsigned integer (8 bit) source buffer.
	* @return The error code
	*/
	int Write_LMH0384_General_Control_Register(uint8_t* data);
	
	/**
	* Set the value from buffer "data" to Output Driver register
	* @param data an unsigned integer (8 bit) source buffer.
	* @return The error code
	*/
	int Write_LMH0384_Output_Driver_Register(uint8_t* data);
	
	/**
	* Set the value from buffer "data" to Launch Amplitude register
	* @param data an unsigned integer (8 bit) source buffer.
	* @return The error code
	*/
	int Write_LMH0384_Launch_Amplitude_Register(uint8_t* data);
	
	
	// ===== Read registers =====
	
	/**
	* Read the value of General Control register register
	* @param buf an unsigned integer (8 bit) buffer where data is read to.
	* @return The error code
	*/
	int Read_LMH0384_General_Control_Register(uint8_t* buf);
	
	/**
	* Read the value of Output Driver register register
	* @param buf an unsigned integer (8 bit) buffer where data is read to.
	* @return The error code
	*/
	int Read_LMH0384_Output_Driver_Register(uint8_t* buf);
	
	/**
	* Read the value of Launch Amplitude register
	* @param buf an unsigned integer (8 bit) buffer where data is read to.
	* @return The error code
	*/
	int Read_LMH0384_Launch_Amplitude_Register(uint8_t* buf);
	
	/**
	* Read the value of CLI register; bits: (7, 6, 5, 4, 3)
	* @param buf an unsigned integer (8 bit) buffer where data is read to.
	* @return The error code
	*/
	int Read_LMH0384_CLI_Register(uint8_t* buf);
	
	/**
	* Read the value of Device ID register; bits: (7, 6, 5, 4, 3, 2, 1, 0)
	* @param buf an unsigned integer (8 bit) buffer where data is read to.
	* @return The error code
	*/
	int Read_LMH0384_DevID_Register(uint8_t* buf);
	
	
	// ===== Read\write regster fields =====
	
	/**
	* Read the value of General Control register Carrier Detect field; bits: (7)
	* @param buf an unsigned integer (8 bit) buffer where data is read to.
	* @return The error code
	*/
	int Read_LMH0384_GnrlCtrl_CD(uint8_t* buf);
	
	/**
	* Write data from "data" buffer to General Control register Carrier Detect field; bits: (7)
	* @param data an unsigned integer (8 bit) buffer where data is written from.
	* @return The error code
	*/
	int Write_LMH0384_GnrlCtrl_CD(uint8_t* data);
	
	/**
	* Read the value of General Control register Mute field; bits: (6)
	* @param buf an unsigned integer (8 bit) buffer where data is read to.
	* @return The error code
	*/
	int Read_LMH0384_GnrlCtrl_Mute(uint8_t* buf);
	
	/**
	* Write data from "data" buffer to General Control register Mute field; bits: (6)
	* @param data an unsigned integer (8 bit) buffer where data is written from.
	* @return The error code
	*/
	int Write_LMH0384_GnrlCtrl_Mute(uint8_t* data);
	
	/**
	* Read the value of General Control register Bypass field; bits: (5)
	* @param buf an unsigned integer (8 bit) buffer where data is read to.
	* @return The error code
	*/
	int Read_LMH0384_GnrlCtrl_Bypass(uint8_t* buf);
	
	/**
	* Write data from "data" buffer to General Control register Bypass field; bits: (5)
	* @param data an unsigned integer (8 bit) buffer where data is written from.
	* @return The error code
	*/
	int Write_LMH0384_GnrlCtrl_Bypass(uint8_t* data);
	
	/**
	* Read the value of General Control register Sleep Mode field; bits:  (4, 3)
	* @param buf an unsigned integer (8 bit) buffer where data is read to.
	* @return The error code
	*/
	int Read_LMH0384_GnrlCtrl_SlpMode(uint8_t* buf);
	
	/**
	* Write data from "data" buffer to General Control register Sleep Mode field; bits: (4, 3)
	* @param data an unsigned integer (8 bit) buffer where data is written from.
	* 00 - disable
	* 01 - sleep mode's active when no input signal detected
	* 10 - force equqlizer into sleep mode
	* @return The error code
	*/
	int Write_LMH0384_GnrlCtrl_SlpMode(uint8_t* data);
	
	/**
	* Read the value of General Control register Extended 3G Reach Mode field; bits:  (2)
	* @param buf an unsigned integer (8 bit) buffer where data is read to.
	* @return The error code
	*/
	int Read_LMH0384_GnrlCtrl_Extended_3G_reach_mode(uint8_t* buf);
	
	/**
	* Write data from "data" buffer to General Control register Extended 3G Reach Mode field; bits:  (2)
	* @param data an unsigned integer (8 bit) buffer where data is written from.
	* @return The error code
	*/
	int Write_LMH0384_GnrlCtrl_Extended_3G_reach_mode(uint8_t* data);
	
	/**
	* Read the value of Output Driver register Output Swing field; bits: (7,6,5)
	* @param buf an unsigned integer (8 bit) buffer where data is read to.
	* @return The error code
	*/
	int Read_LMH0384_OutputDriver_Output_swing(uint8_t* buf);
	
	/**
	* Write data from "data" buffer to Output Driver register Output Swing field; bits: (7,6,5)
	* @param data an unsigned integer (8 bit) buffer where data is written from.
	* @return The error code
	*/
	int Write_LMH0384_OutputDriver_Output_swing(uint8_t* data);
	
	/**
	* Read the value of Output Driver register Offset Voltage field; bits: (4,3,2)
	* @param buf an unsigned integer (8 bit) buffer where data is read to.
	* @return The error code
	*/
	int Read_LMH0384_OutputDriver_Offset_voltage(uint8_t* buf);
	
	/**
	* Write data from "data" buffer to Output Driver register Offset Voltage field; bits: (4,3,2)
	* @param data an unsigned integer (8 bit) buffer where data is written from.
	* @return The error code
	*/
	int Write_LMH0384_OutputDriver_Offset_voltage(uint8_t* data);
	
	/**
	* Read the value of Launch Amplitude register Coarse Control field; bits: (7)
	* @param buf an unsigned integer (8 bit) buffer where data is read to.
	* 0: Normal optimization with no (800 mVP-P launch amplitude).
	* 1: Optimized for 6 dB external attenuation (400 mVP-P launch amplitude).
	* @return The error code
	*/
	int Read_LMH0384_LaunchAmpld_CoarseCtrl(uint8_t* buf);
	
	/**
	* Write data from "data" buffer to Launch Amplitude register Coarse Control field; bits: (7)
	* @param data an unsigned integer (8 bit) buffer where data is written from.
	* 0: Normal optimization with no (800 mVP-P launch amplitude).
	* 1: Optimized for 6 dB external attenuation (400 mVP-P launch amplitude).
	* @return The error code
	*/
	int Write_LMH0384_LaunchAmpld_CoarseCtrl(uint8_t* data);
	
	/**
	* Read the value of Launch Amplitude register Fine Control field; bits: (6, 5, 4, 3)
	* @param buf an unsigned integer (8 bit) buffer where data is read to.
	* 0000: Nominal.
	* 0001: -4% from nominal.
	* 0010: -8% from nominal.
	* 0011: -11% from nominal.
	* 0100: -14% from nominal.
	* 0101: -17% from nominal.
	* 0110: -20% from nominal.
	* 0111: -22% from nominal.
	* 1000: Nominal.
	* 1001: +4% from nominal.
	* 1010: +9% from nominal.
	* 1011: +14% from nominal.
	* 1100: +20% from nominal.
	* 1101: +26% from nominal.
	* 1110: +33% from nominal.
	* 1111: +40% from nominal.
	* @return The error code
	*/
	int Read_LMH0384_LaunchAmpld_FineCtrl(uint8_t* buf);
	
	/**
	* Write data from "data" buffer to Launch Amplitude register Fine Control field; bits: (6, 5, 4, 3)
	* @param data an unsigned integer (8 bit) buffer where data is written from.
	* 0000: Nominal.
	* 0001: -4% from nominal.
	* 0010: -8% from nominal.
	* 0011: -11% from nominal.
	* 0100: -14% from nominal.
	* 0101: -17% from nominal.
	* 0110: -20% from nominal.
	* 0111: -22% from nominal.
	* 1000: Nominal.
	* 1001: +4% from nominal.
	* 1010: +9% from nominal.
	* 1011: +14% from nominal.
	* 1100: +20% from nominal.
	* 1101: +26% from nominal.
	* 1110: +33% from nominal.
	* 1111: +40% from nominal.
	* @return The error code
	*/
	int Write_LMH0384_LaunchAmpld_FineCtrl(uint8_t* data);

	protected:
	private:

}; //LMH0384_Low_API_Manager

#endif //__LMH0384_LOW_API_MANAGER_H__
