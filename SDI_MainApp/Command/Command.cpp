/* 
* Command.cpp
*
* Created: 11/2/2016 4:25:52 PM
* Author: darya
*/


#include "Command.h"


// default constructor
Command::Command()
{
	read = false;
	chip = (char*)malloc(8);
	chipID = -1;
	reg = (char*)malloc(REG_NAME_LENGTH);
	reg_field = (char*)malloc(REG_NAME_LENGTH);
	value = 0;
} //Command


Command::Command(const char* string): Command(){
	int rw = toRead(string);
	if(rw==1)
		read = true;
	else if (rw==-1)
		rw = false;
	else return;
	
	//Expect correct chip type
	char* cP;
	char* p03 = strstr(string, "LMH0303");
	char* p84 = strstr(string, "LMH0384");
	if(p03!=NULL){
		chip = (char*)"LMH0303";
		cP = p03+6;
	}
	else if(p84!=NULL){
		chip = (char*)"LMH0384";
		cP = p84+6;
	}
	else return;	
	
	//Expect chipID
	while(*(++cP) == ' ');
	if(*(cP) == '-'){
		cP++;
		int id = (*cP)=='1'?1: (*cP)=='2'?2: (*cP)=='3'?3: (*cP)=='4'?4: (*cP)=='5'?5: -1;
		if(id < 0) return;
		chipID = id;
	}
	//Expect register name
	while(*(++cP) == ' ');
	char* p = cP;
	while((*(cP)!='.') && (*(cP)!=' '))
		cP++;	
	//reg = (char*)malloc(cP-p+1);
	strncpy(reg, p, cP-p);
	//If there is register.field
	if(*(cP)=='.'){
		p = ++cP;
		while(*(cP) != ' ')
			cP++;
		//reg_field = (char*)malloc(cP-p+1);
		strncpy(reg_field, p, cP-p);
	}
	//If "write" expect hexadecimal value
	while(*(++cP) == ' ');

	char subStr[9];
	strncpy(subStr, cP, 8);
	parse_hex(subStr, &value);
			
}

int Command::parse_hex(char* s, uint8_t* h){
	const char*  p;
	uint8_t n;
	while(*s == ' ') s++;
	
	if((*s != '0') || ((*(s + 1) & 0xDF) != 'X'))
	return -1;
	
	for(n = 0, p = s + 2; *p; ++p){
		if(*p >= '0' && *p <= '9')
		n = (n << 4) | (uint8_t)(*p - '0');
		else if(*p >= 'A' && *p <= 'F')
		n = (n << 4) | (uint8_t)(*p - 'A' + 10);
		else
		break;
	}
	
	
	if(((int)(p - s) - 2) > (int)(sizeof(n) << 1))
	return -1;
	
	*h = n;
	return (p != s + 2) ? (int)(p - s) : 0;
}

int Command::toRead(const char* string){
	if(strstr(string, "read") != NULL)
	return 1;
	else if(strstr(string, "write") != NULL)
	return -1;
	else
	return -49;
}

void Command::ToString(char* cmd_str){
	char* empty = (char*)malloc(REG_NAME_LENGTH);
	if(read){
		strcat(cmd_str, "read ");}
	else {
		strcat(cmd_str, "write ");}
	strcat(cmd_str, chip);
	strcat(cmd_str, "-");
	
	//Write chipID to the command
	char id[2];
	itoa(chipID, id, 10);
	strncat(cmd_str, id, 1);
	
	strcat(cmd_str, " ");
	strcat(cmd_str, reg);
	if(strcmp(reg_field, empty) != 0){
		strcat(cmd_str, ".");
		strcat(cmd_str, reg_field);
	}
	strcat(cmd_str, " ");
	//Write value to the command

	char* hex = (char*)malloc(5);
	inttohex(value, hex);
	strncat(cmd_str, hex, 4);
	
	delete empty;
	delete hex;
}

int Command::inttohex(uint8_t dec, char* hex)
{    
	*(hex) = '0';
	*(hex+1) = 'x';
	char digits[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
	*(hex+3) = digits[dec % 16];
	dec /= 16;
	*(hex+2) = digits[dec % 16];
	dec /= 16;
	if(dec == 0)
		return	0;
	else
		return -dec;
}

// default destructor
Command::~Command()
{
	delete chip;
	delete reg;
	delete reg_field;
} //~Command
