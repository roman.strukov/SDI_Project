/* 
* Command.h
*
* Created: 11/2/2016 4:25:52 PM
* Author: darya
*/


#ifndef __COMMAND_H__
#define __COMMAND_H__

#include <string.h>
#include <stdint.h>
#include <asf.h>
#include "../new/new.h"

#define CMD_LENGTH 76
#define REG_NAME_LENGTH 25

/*!
 * \brief Class that contains a field for each part of response/request string.
 */
class Command
{
//variables
public:
	bool read;			/**<If true - read command, else - write command. */
	char* chip;			/**<Chip type: "LMH0303" or "LMH0384". */
	int chipID;			/**<Chip id: from 1 to 4 - for real chips, 5 - for test chip. */
	char* reg;			/**<Register name. List of names is unique for every type of chip. */
	char* reg_field;	/**<Register field name. List of fields is unique for every register. */
	uint8_t value;		/**<Value to write or read value. */
protected:
private:

//functions
public:
	Command();
	/**
	* Form an object from const char array.
	* @param string Const char array from what object of command class will be formed.
	* @return An object of the Command class.
	*/
	Command(const char* string);
	/**
	* Form char array in specific format from fields of current object. Write this array on the address of "cmd_str".
	* @param cmd_str An address where formed char array will be written to.
	*/
	void ToString(char* cmd_str);
	~Command();
protected:
private:
	int parse_hex(char* s, uint8_t* h);
	int inttohex(uint8_t a, char* tmp);
	int toRead(const char* string);
	Command( const Command &c );
	Command& operator=( const Command &c );

}; //Command

#endif //__COMMAND_H__
