/*
 * main.cpp
 *
 * Created: 12/15/2016 3:41:42 PM
 *  Author: darya
 */ 
#include "../SDI_Manager/SDI_Manager.h"
#include "m.h"

static volatile bool main_b_cdc_enable = false;

int main(void)
{
	int error = 0;
	irq_initialize_vectors();
	cpu_irq_enable();

	// Initialize the sleep manager
	sleepmgr_init();

	#if !SAM0
		sysclk_init();
		board_init();
	#else
		system_init();
	#endif

	#ifdef _cplusplus
	 extern "C"{
		ui_init();
		ui_powerdown();

		// Start USB stack to authorize VBus monitoring
		udc_start();

		SDI_Manager::Initialize();
		error = TWI_Chip::changeDefaultAddresses();
		// The main loop manages the power mode
		// because the USB management is done by interrupt
		while (true) {
			sleepmgr_enter_sleep();
			char str_cmd[CMD_LENGTH];
			read(str_cmd, CMD_LENGTH);
			Command* cmd = new Command(str_cmd);
			SDI_Manager::parseCommand(cmd);
			char str_res[CMD_LENGTH];
			cmd->ToString(str_res);
			write(str_cmd, CMD_LENGTH);
		}
	};
#endif // _cplusplus
	return error;
}
