/* 
* UnitTest.cpp
*
* Created: 11/10/2016 9:23:58 AM
* Author: darya
*/


#include "UnitTest.h"

// default constructor
UnitTest::UnitTest()
{
	sdiManager = new SDI_Manager();
} //UnitTest

void UnitTest::Initialize(){
	//SDI_Manager::Initialize();
	sdiManager->Initialize();
}
int UnitTest::TestRequstsToLMH0303(){
	Command* cmd = new Command();
	cmd->read = true;
	cmd->chip = (char*)"LMH0303";
	cmd->chipID = 5;
	cmd->reg = (char*)"DeviceID";
	int error1 = sdiManager->parseCommand(cmd);
	cmd->reg = (char*)"Status";
	int error2 = sdiManager->parseCommand(cmd);	
	cmd->reg = (char*)"Mask";
	int error3 = sdiManager->parseCommand(cmd);	
	cmd->reg = (char*)"Direction";
	int error4 = sdiManager->parseCommand(cmd);
	cmd->reg = (char*)"Output";
	int error5 = sdiManager->parseCommand(cmd);	
	cmd->reg = (char*)"OutputCTRL";
	int error6 = sdiManager->parseCommand(cmd);
	cmd->reg = (char*)"RSVD06";
	int error7 = sdiManager->parseCommand(cmd);	
	cmd->reg = (char*)"RSVD07";
	int error8 = sdiManager->parseCommand(cmd);	
	cmd->reg = (char*)"Test";
	int error9 = sdiManager->parseCommand(cmd);	
	cmd->reg = (char*)"Rev";
	int error10 = sdiManager->parseCommand(cmd);	
	cmd->reg = (char*)"TFNCount";
	int error11 = sdiManager->parseCommand(cmd);	
	cmd->reg = (char*)"TFPCount";
	int error12 = sdiManager->parseCommand(cmd);	
	
	return error1!=0?error1: error2!=0?error2: error3!=0?error3: error4!=0?error4: error5!=0?error5: error1!=6?error6: error7!=0?error7: error8!=0?error8: error9!=0?error9: error10!=0?error10: error11!=0?error11: error12;
}

int UnitTest::TestRequstsToLMH0384(){
		Command* cmd = new Command();
		cmd->read = true;
		cmd->chip = (char*)"LMH0384";
		cmd->chipID = 5;
		cmd->reg = (char*)"General Control";
		int error1 = sdiManager->parseCommand(cmd);
		cmd->reg = (char*)"Output Driver";
		int error2 = sdiManager->parseCommand(cmd);
		cmd->reg = (char*)"Launch Amplitude";
		int error3 = sdiManager->parseCommand(cmd);
		cmd->reg = (char*)"CLI";
		int error4 = sdiManager->parseCommand(cmd);
		cmd->reg = (char*)"Device ID ";
		int error5 = sdiManager->parseCommand(cmd);
		
		return error1!=0?error1: error2!=0?error2: error3!=0?error3: error4!=0?error4: error5;
}

// default destructor
UnitTest::~UnitTest()
{
} //~UnitTest
